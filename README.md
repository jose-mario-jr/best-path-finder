# BestPathFinder

- Here we have the implementation of a service that takes an origin and a destination and find the way that crosses less borders possible

# Functionalities

- The endpoint /routing/{origin}/{destination} returns the route with less land crossings possible;
- If there is no valid route, the server returns error with status code 400;
- The parameters should be cca3 country codes, if not, the server returns error with status code 404;
- If the user tries to access any other route other than this one, the server returns error message status 404.

# Technologies used

- Node.js with http package

# Setup and testing

- Run ```npm start``` on terminal and that's it
- Tests can be done by VsCode's Rest Client with the test.http file